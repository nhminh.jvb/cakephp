/* 
 * Author: Huy
 * Created Date: 2018/08/31
 * Payment Calculation
 */

$(function () {
    if ($("input[class='js-bonus-addition']").length > 0) {
        onTriggerBonusAddition();
        
        $("input[class='js-bonus-addition']").change(function () {
            onTriggerBonusAddition();
        });
    }
});

/**
 * ボーナス加算が「有」であれば、「ボーナス支払月」と「ボーナス加算支払金」項目が表示される
 */
function onTriggerBonusAddition() {
    var selectedVal = $("input[class='js-bonus-addition']:checked").val();
    switch (selectedVal) {
        case "1":
            $('.bonus-addition').show();
            break;
        default:
            $('.bonus-addition').hide();
            break;
    }
}
