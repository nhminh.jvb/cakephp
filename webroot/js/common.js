/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {
    if ($('.datepicker').length > 0) {
        $('.datepicker').each(function (i, el) {
            var v = $(this).val();
            $(this).datepicker();
            $(this).datepicker('option', 'dateFormat', 'yy/mm/dd');
            $(this).datepicker('setDate', v);	//デフォルト日付セット
        });
    }

    if ($("input[name='type']").length > 0) {
        onTriggerType();
        
        $("input[name='type']").change(function () {
            onTriggerType();
        });
    }

});

/**
 * 属性が変わった時、該当な属性の処理を行う
 */
function onTriggerType() {
    var selectedVal = $("input[name='type']:checked").val();
    switch (selectedVal) {
        case "1":
            $('.work-info').show();
            $('.pension').hide();
            break;
        case "4":
            $('.pension').show();
            $('.work-info').hide();
            break;
        default:
            $('.pension').hide();
            $('.work-info').hide();
            break;
    }
}

//popupを開く
function popOpen(url, title, width, height) {
    var win = window.open(url, title, 'toolbar=no, location=no, status=no, menubar=no,resizable=yes, scrollbars=yes, width=' + width + ', height=' + height);
}
