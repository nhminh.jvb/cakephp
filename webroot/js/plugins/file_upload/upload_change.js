var uploadButton = '......';
//uploadButton = '<div class="progress progressImg"><div class="progress-bar progress-bar-success"></div></div>';
function uploadDocImage(url, uploadError) {
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: true,
        disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent)
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').addClass('div-file-upload').appendTo('#filesViewImg');
        $.each(data.files, function (index, file) {
            var node = $('<span/>').text(file.name);
            node.append(uploadButton);
            node.appendTo(data.context);
        });
    }).on('fileuploadprogressall', function (e, data) {
//            var progress = parseInt(data.loaded / data.total * 100, 10);
//            $('.progressImg .progress-bar').css('width',progress + '%');
    }).on('fileuploaddone', function (e, data) {
        if (data.result.code == 1) {
            var link = $('<a>').attr({'class': 'fancybox', 'rel': "1"+ data.result.filename}).prop('href', data.result.href);
            $(data.context).find('span').remove();
            $(data.context).append('<img src="' + data.result.href + '" width="100px;">');
            $(data.context.children()).wrap(link);
            $(data.context).append('<button data-image="image" data-type="tmp" data-file="' + data.result.filename + '" type="button" class="btn btn-sm btn-default-2 deleteImg">削除</button>');
            $(data.context).append('<input type="hidden" name="personal_id_document_img_arr[]" value="' + data.result.filename + '">');
            loadFancyBox();
        } else if (data.result.message) {
            var error = $('<span class="text-danger"/>').text(data.result.message);
            $(data.context).append(error);
        }
    }).on('fileuploadfail', function (e, data) {
        var error = $('<span class="text-danger"/>').text(uploadError);
        $(data.context).append(error);
    }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
}

function uploadDocOther(url, uploadError) {
    $('#fileOtherUpload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: true,
        disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent)
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').addClass('div-file-upload').appendTo('#filesViewOther');
        $.each(data.files, function (index, file) {
            var node = $('<span/>').text(file.name);
            node.append(uploadButton);
            node.appendTo(data.context);
        });
    }).on('fileuploadprogressall', function (e, data) {
//            var progress = parseInt(data.loaded / data.total * 100, 10);
//            $('.progressImg .progress-bar').css('width',progress + '%');
    }).on('fileuploaddone', function (e, data) {
        if (data.result.code == 1) {
            if(data.result.isImg){
                var link = $('<a>').attr({'class': 'fancybox', 'rel': "2"+ data.result.filename}).prop('href', data.result.href);
                $(data.context).append('<img src="' + data.result.href + '" width="100px;">');
                $(data.context).find('span').remove();
            }else{
                var link = $('<a>').attr({'target': '_blank'}).prop('href', data.result.href);
                $(data.context).find('span').text(data.result.filename);
            }
            $(data.context.children()).wrap(link);
            $(data.context).append('<button data-image="other" data-type="tmp" data-file="' + data.result.filename + '" type="button" class="btn btn-sm btn-default-2 deleteImg">削除</button>');
            $(data.context).append('<input type="hidden" name="other_document_img_arr[]" value="' + data.result.filename + '">');
            loadFancyBox();
        } else if (data.result.message) {
            var error = $('<span class="text-danger"/>').text(data.result.message);
            $(data.context).append(error);
        }
    }).on('fileuploadfail', function (e, data) {
        var error = $('<span class="text-danger"/>').text(uploadError);
        $(data.context).append(error);
    }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
}


function loadFancyBox() {
    $(".fancybox").fancybox({
        openEffect: 'fade',
        closeEffect: 'fade',
    });
}