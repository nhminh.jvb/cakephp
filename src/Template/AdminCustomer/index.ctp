
<div class="text-center">List Customer</div>
<?= $this->Form->create("",['type'=>'get']) ?>
<div class="col-md-5">
   <?= $this->Form->control('key'); ?>
   <button>Serach</button>
</div>
<?= $this->Form->end() ?>
 <table class="table-bordered col-md-10">
      <?php foreach ($data as $data): ?>
       <tr>
            <td><?= $data->name ?></td>
            <td><?= $data->age ?></td>
            <td><?= $data->gender ?></td>
            <td ><?= $data->phone ?></td>
            <?php if($level === 1): ?>
            <td><button type="button" onclick="location.href = '<?= $this->Url->build(['action' => 'edit',$data->id]); ?>'">edit</button> </td>
            <?php endif; ?>
            <td><button type="button" onclick="location.href = '<?= $this->Url->build(['action' => 'create']); ?>'">add</button> </td>
            <td><button type="button" onclick="location.href = '<?= $this->Url->build(['controller'=>'AdminContract','action' => 'create', $data->id]); ?>'">create contract</button> </td>
        </tr>
          <?php endforeach; ?>
    </table>

<button type="button" onclick="location.href = '<?= $this->Url->build(['controller'=>'AdminLogin','action' => 'logout']); ?>'">Log out</button>
<?php  
    echo $this->Paginator->prev('<< '.__('previous',true), array(), null, array('class'=>'disabled'));
    echo $this->Paginator->numbers();
    echo $this->Paginator->next(__('next',true).' >>',  array(), null, array('class'=>'disabled'));
  ?>