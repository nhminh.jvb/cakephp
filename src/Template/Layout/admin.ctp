<html>
    <head>
        <?= $this->Html->charset() ?>
        <?= $this->Html->meta('icon') ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
         <!-- plugins css -->
        <?= $this->Html->css('plugins/jquery_ui/themes/base/jquery.ui.all.css') ?>
        <?= $this->Html->css('plugins/bootstrap-4.1.2/css/bootstrap.min.css') ?>
       
      <!-- custom css -->
        <?= $this->Html->css(['base.css', 'home.css', 'style.css']) ?>
        
        <?= $this->Html->script('plugins/jquery-3.3.1/js/jquery-3.3.1.min.js'); ?>
        <?= $this->Html->script('plugins/jquery_ui/ui/jquery.ui.core.js'); ?>
        <?= $this->Html->script('plugins/jquery_ui/ui/jquery.ui.datepicker.js'); ?>
        <?= $this->Html->script('plugins/bootstrap-4.1.2/js/bootstrap.bundle.min.js'); ?>
      
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <?= $this->Flash->render() ?>
                    <?= $this->fetch('content'); ?>
    </body>
    
</html>
