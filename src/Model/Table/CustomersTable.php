<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;
/**
 * Description of CustomersTable
 *
 * @author Admin
 */
class CustomersTable extends Table{
    public function initialize(array $config)
    {
         $this->hasMany('Contracts', [
            'foreignKey' => 'customer_id',
            'className' => 'Contracts',
        ]);
    }
    
    public function validationDefault(Validator $validator) {

        $validator
                ->notEmpty('name', 1);
        $validator
                ->notEmpty('phone', 2);
       
        return $validator;
    }
}
