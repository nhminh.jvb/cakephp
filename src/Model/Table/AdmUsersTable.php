<?php


/**
 * Author: Thang
 * Created at 2018/09/05
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;

class AdmUsersTable extends Table {

    public function validationDefault(Validator $validator) {

        $validator
                ->notEmpty('name', 1);
        $validator
                ->requirePresence('phone_number', 'create')
                ->notEmpty('phone_number', 2);
               
        $validator
                ->requirePresence('email', 'create')
                ->notEmpty('email', 3);
               

        $validator
                ->notEmpty('login_id', 4);
                
        $validator
                ->requirePresence('login_pw', 'create')
                ->notEmpty('login_pw', 5);
               
        return $validator;
    }

    public function validationLogin(Validator $validator) {

        $validator->provider("custom", 'App\Validation\CustomValidation');

        $validator
                ->notEmpty('name', 6);
                
        $validator
                ->notEmpty('login_pw', 7);
                
        $validator
                ->notEmpty('email', 8);
                
        return $validator;
    }

    public function findAuth(\Cake\ORM\Query $query, array $options) {
        $query
                ->select(['id', 'name', 'phone_number', 'email', 'login_id', 'login_pw']);
//                ->where([
//                    'AdmUsers.is_deleted' => 0,
//        ]);
        return $query;
    }
    
    /*
     * get info in AdmUsers table
     * @param type $post_data
     * @return array
     */
//    public function getForgetAdmUserInfo($post_data) {
//        $data = $this->find()
//                ->where([
//                    'is_deleted' => 0,
//                    'login_id' => $post_data["login_id"],
//                    'email' => $post_data["email"],
//                ])
//                ->select(["id", "phone_number", "email", "login_id", "login_pw"])
//                ->first();
//        return $data;
//    }
//
//    /**
//     * check unique of field login_id
//     * @param type $login_id
//     * @return boolean
//     */
//    public function LoginIDUnqCheck($login_id, $context) {
//        $data = $this->find()
//                ->where(['AdmUsers.is_deleted' => 0, 'AdmUsers.login_id' => $login_id,]);
//
//
//        if (!empty($context['data']['id'])) {
//            $data = $data->where([
//                'AdmUsers.id !=' => $context['data']['id'],
//            ]);
//        }
//
//        $data = $data->first();
//
//        if (!empty($data)) {
//            return false;
//        } else {
//            return true;
//        }
//    }

}
