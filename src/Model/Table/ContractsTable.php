<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;
/**
 * Description of ContractsTable
 *
 * @author Admin
 */
class ContractsTable extends Table{
   public function initialize(array $config)
    {
           $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'LEFT',
            'className' => 'Customers',
          ]);
    }
    
    public function validationDefault(Validator $validator) {

        $validator
                ->notEmpty('name', 1);
       
        return $validator;
    }
}
