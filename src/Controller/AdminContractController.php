<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Description of AdminContractController
 *
 * @author min
 */
class AdminContractController extends AppController {
     public function beforeRender(Event $event) {
        parent::beforeRender($event);
         $this->viewBuilder()->setLayout('admin');
    }
    
    public function index() {
        
    }
    public function create() {
        $minh = $this->loadModel('Contracts');
        $data = $this->Contracts->newEntity();
        if ($this->request->is('post')) {
            $data = $this->Contracts->newEntity($this->request->data);
            if($this->Contracts->save($data)){
                return $this->redirect(['action'=>'index']);
            }
        }
        $this->set(compact('data'));
    }
    
}
