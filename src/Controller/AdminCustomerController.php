<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * Description of AdminCustomerController
 *
 * @author Admin
 */
class AdminCustomerController extends AppController {
    public function beforeRender(Event $event) {
        parent::beforeRender($event);
        $this->viewBuilder()->setLayout('admin');
    }
    
    public $paginate = [
        'limit' => 2,
        
    ];
    
    public function index() {
        $this->loadModel('Customers');
        $query = $this->Customers->find('all')
                ->select(['id','name','age','gender','phone']);
        $keyword = $this->request->query('key');
        if(!empty($keyword)){
          $query -> where(['name LIKE'=>'%'.$keyword.'%']);
        }
        
        $data = $this->paginate($query);
        $session = $this->request->session()->read('Auth.User.level');
        $level = 0;
        if($session == 1){
            $level = 1;
        }
        
       
        $this->set(compact('data','level'));
    }
    public function create() {
        $this->loadModel('Customers');
        $data = $this->Customers->newEntity();
        if ($this->request->is('post')) {
            $this->loadModel('Customers');
            $data = $this->Customers->newEntity($this->request->data);
            if($this->Customers->save($data)){
                return $this->redirect(['action'=>'index']);
            }
        }
        $this->set(compact('data'));
    }
    
    public function edit($id) {
       $this->loadModel('Customers');
        $query = $this->Customers->find()
                ->select(['id','name','age','gender','phone'])
                ->where(['id'=>$id])
                ->first();
        if(!$query){
             return $this->redirect(['action'=>'index']);
        }
        $data = $this->Customers->newEntity();
        if($this->request->is('post')){
            $data = $this->Customers->newEntity($this->request->data);
            $data->id = $query->id;
            if($this->Customers->save($data)){
                return $this->redirect(['action'=>'index']);
            }
        }
      $this->set(compact('data','query'));
    }
}
