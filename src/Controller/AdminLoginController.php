<?php
/**
 * Author: Viet
 * Created at 2018/09/05
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Mailer\Email;

class AdminLoginController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['register']);
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);
        $this->viewBuilder()->setLayout('admin');
    }
  
    /**
     * login
     * @return type
     */
    public function index() {
        $this->loadModel('Users');
        $AdmUsers = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $this->loadModel('Users');
            $AdmUsers = $this->Users->newEntity($this->request->data(), ['validate' => 'login']);
            if (!$AdmUsers->getErrors()) {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    return $this->redirect(['controller'=>'AdminCustomer','action'=>'index']);
                }
                $this->Flash->set('Not Succelly');
            }
        }
        $this->set(compact('AdmUsers'));
    }

    public function register() {
        $this->loadModel('Users');
        $data = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $this->loadModel('Users');
            $data = $this->Users->newEntity($this->request->data);
            if($this->Users->save($data)){
                return $this->redirect(['action'=>'index']);
            }
            return $this->redirect(['action'=>'register']);
        }
        $this->set(compact('data'));
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

}